<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/siswa', 'MasterData\MasterDataController@siswa');
Route::get('/pegawai', 'MasterData\MasterDataController@pegawai');

Route::get('/masuk', 'Keuangan\KeuanganController@masuk');
Route::get('/keluar', 'Keuangan\KeuanganController@keluar');
Route::get('/rekap', 'Keuangan\KeuanganController@rekap');

