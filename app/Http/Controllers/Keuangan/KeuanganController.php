<?php

namespace App\Http\Controllers\Keuangan;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class KeuanganController extends Controller
{
    public function masuk(){
    	return view('Keuangan.masuk');
    }

    public function keluar(){
    	return view('Keuangan.keluar');
    }

     public function rekap(){
    	return view('Keuangan.rekap_keuangan');
    }
}
