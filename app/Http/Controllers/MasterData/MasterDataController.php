<?php

namespace App\Http\Controllers\MasterData;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MasterDataController extends Controller
{
    public function siswa(){
    	return view('MasterData.siswa');
    }

    public function pegawai(){
    	return view('MasterData.pegawai');
    }
}
